# Deploy_aws_eks
Deploiement eks sur aws
# Terraform Project

This project contains the necessary files to deploy EKS infrastructure on AWS using Terraform.

## Prerequisites

Before you begin, ensure you have met the following requirements:
- Install Terraform (version 0.12 or later)
- Configure your AWS credentials

## Usage

To deploy the infrastructure, follow these steps:

1. Initialize Terraform:
   ```
   terraform init
   ```

2. Format the configuration files:
   ```
   terraform fmt
   ```

3. Validate the configuration files:
   ```
   terraform validate
   ```

4. Plan the changes to be made:
   ```
   terraform plan
   ```

5. Apply the changes:
   ```
   terraform apply
   ```
