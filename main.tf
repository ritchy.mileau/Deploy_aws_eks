terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~> 2.21.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "2.10.1"
    }


  }
}

data "aws_eks_cluster_auth" "cluster" {
  count = var.create ? 1 : 0
  name  = module.eks.cluster_name
}

provider "kubernetes" {
  host                   = module.eks.cluster_endpoint
  cluster_ca_certificate = base64decode(module.eks.cluster_certificate_authority_data)
  token                  = element(concat(data.aws_eks_cluster_auth.cluster[*].token, tolist([""])), 0)
  exec {
    api_version = "client.authentication.k8s.io/v1beta1"
    command     = "aws"
    # This requires the awscli to be installed locally where Terraform is executed
    args = ["eks", "get-token", "--cluster-name", module.eks.cluster_name]
  }
}

provider "helm" {
  kubernetes {
    host                   = module.eks.cluster_endpoint
    cluster_ca_certificate = base64decode(module.eks.cluster_certificate_authority_data)
    token                  = element(concat(data.aws_eks_cluster_auth.cluster[*].token, tolist([""])), 0)
  }
}

provider "aws" {
  region = var.region
}

#recupere la liste des azs
data "aws_availability_zones" "available" {}

locals {
  cluster_name    = "cluster-eks-taravasysnet"
  cluster_version = "1.25"
}

#appel d'un module vpc sur terraform registry
module "vpc" {
  source = "./modules/vpc"
  azs    = slice(data.aws_availability_zones.available.names, 0, 3)
  # cidr            = "10.0.0.0/16"
  public_subnets  = var.public_subnets
  private_subnets = var.private_subnets
}

# appel d'un module eks cluster sur terraform registry
module "eks" {
  source = "./modules/eks"



  cluster_name    = local.cluster_name
  cluster_version = local.cluster_version

  vpc_id     = module.vpc.vpc_id
  subnet_ids = module.vpc.private_subnets

  eks_managed_node_group_defaults = var.eks_managed_node_group_defaults

  eks_managed_node_groups = var.eks_managed_node_groups

}
module "ebs" {
  source     = "./modules/ebs"
  depends_on = [module.eks]


  oidc_providers = {
    main = {
      provider_arn               = module.eks.oidc_provider_arn
      namespace_service_accounts = ["default:ebs-csi-controller-sa","aws-ebs-csi-driver:ebs-csi-controller-sa"]
    }

  }
}



