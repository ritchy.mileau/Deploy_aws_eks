resource "helm_release" "this" {
  name             = "aws-ebs-csi-driver"
  repository       = "https://kubernetes-sigs.github.io/aws-ebs-csi-driver"
  chart            = "aws-ebs-csi-driver"
  namespace        = "aws-ebs-csi-driver"
  create_namespace = true

  set {
    name  = "controller.serviceAccount.annotations.eks\\.amazonaws\\.com/role-arn"
    value = "${module.iam_iam-role-for-service-accounts-eks.iam_role_arn}"
  }

  set {
    name = "storageClasses[0].name"
    value = "ebs-sc"
  }

  set {
    name = "storageClasses[0].volumeBindingMode"
    value = "WaitForFirstConsumer"
  }

}


