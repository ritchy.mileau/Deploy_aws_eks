variable "cluster_endpoint" {
  type        = string
  default     = ""
  description = "description"
}

variable "cluster_ca_cert" {
  type        = string
  default     = ""
  description = "description"
}

variable "cluster_name" {
  type        = string
  default     = ""
  description = "description"
}

variable "oidc_providers" {
  type        = any
  default     = ""
  description = "oidc_providers"
}

variable "annotations" {
  type        = string
  default     = ""
  description = "description"
}
