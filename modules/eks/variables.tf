variable "cluster_name" {
  description = "Name of the EKS cluster"
  type        = string
}

variable "cluster_version" {
  type        = string
  description = "EKS Cluster Version"
}

variable "vpc_id" {
  description = "EKS vpc id"
}

variable "subnet_ids" {
  description = "Liste des subnets provisionnés"
  type        = list(string)
  default     = []
}

variable "eks_managed_node_group_defaults" {
  default     = {}
  description = "Configuration par défaut EKS managed group"
}

variable "eks_managed_node_groups" {
  description = "Definitions des EKS managed node groups"
  default     = {}
}


