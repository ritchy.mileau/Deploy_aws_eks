variable "name_vpc" {
  type        = string
  default     = ""
  description = "vpc name"
}

variable "cidr_vpc" {
  type        = string
  default     = "10.0.0.0/16"
  description = "vpc cidr"
}

variable "private_subnets" {
  type        = list(string)
  default     = ["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"]
  description = "vpc private subnets"
}

variable "public_subnets" {
  type        = list(string)
  default     = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  description = "vpc public subnets"
}

variable "cluster_name" {
  type        = string
  default     = ""
  description = "vpc cluster name"
}

variable "azs" {

}
