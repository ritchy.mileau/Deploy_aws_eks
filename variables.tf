variable "region" {
  description = "AWS region"
  type        = string
  default     = "us-west-2"
}

variable "subnet_ids" {
  description = "Liste des subnets provisionnés"
  type        = list(string)
  default     = []
}

variable "private_subnets" {
  type        = list(string)
  default     = ["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"]
  description = "vpc private subnets"
}

variable "public_subnets" {
  type        = list(string)
  default     = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  description = "vpc public subnets"
}

variable "eks_managed_node_group_defaults" {
  default = {
    ami_type = "AL2_x86_64"

  }
  description = "Configuration par défaut EKS managed group"
}

variable "eks_managed_node_groups" {
  description = "Definitions des EKS managed node groups"
  default = {
    one = {
      name = "node-group-1"

      instance_types = ["t3.small"]

      min_size     = 1
      max_size     = 3
      desired_size = 2
    }

    two = {
      name = "node-group-2"

      instance_types = ["t3.small"]

      min_size     = 1
      max_size     = 2
      desired_size = 1
    }
  }
}

variable "create" {}
